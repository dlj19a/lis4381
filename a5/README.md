# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 1 Requirements:

*Three Parts:*

1. Server Side Validation
2. Skillsets 13-15
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of Passing Validation
* Screenshot of Failed Validation
* Screenshot of running Android Studio
* Git commands with short descriptions
* Bitbucket repo links: a)this assignment and b) the completed tutorials above

#### Assignment Screenshots:

*Screenshot of Data Table*:

![Data Table](img/data_table.png)

*Screenshot of invalid data*:

![Invalid Data Screenshot](img/server_validation.png)

*Screenshot of failed validation*:

![Failed Validation Screenshot](img/server_failed.png)

*Screenshot of Valid Data*:

![Valid Data Screenshot](img/passing_server.png)

*Screenshot of added Data*:

![Added Data Screenshot](img/added_data.png)

*Screenshot of Skillset 13*:

![Skillset 13 Screenshot](img/skillset13.png)

*Screenshot of Addition Screen*:

![Addition Screenshot](img/additionscreen.png)

*Screenshot of Addition Calculated*:

![Addition Calculated Screenshot](img/addition_calculated.png)

*Screenshot of Division Screen*:

![Division Screenshot](img/division_screen.png)

*Screenshot of Division Calculated*:

![Division Screenshot](img/division_calculated.png)

*Screenshot of Comment Screen*:

![Comment Screen Screenshot](img/comment.png)

*Screenshot of Gettysburg Address*:

![Gettysburg Address Screenshot](img/gettysburg.png)


#### Tutorial Links:

*Link to Local LIS4381 A5:*
[Local Host A5 Link](http://localhost:8080/repos/lis4381/a5/index.php "A5 Local Host Link")


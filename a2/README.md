# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 2 Requirements:

*Three Parts:*

1. Mobile Recipe App Creation with Android Studio
2. Changing app interface and adding buttons
3. Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Screenshot of running application's first user interface.
* Screenshot of running application's second user interface.
* Screenshot of each skillset (1-3).

#### Assignment Screenshots:

| *Screenshot of first user interface*:          | *Screenshot of second user interface*:      |
|--------------|-----------|
| ![First User Interface Screenshot](img/first_interface.png "My First User Interface Screenshot") | ![Second User Interface Screenshot](img/second_interface.png "My Second User Interface Screenshot")      |

| *Screenshot of Even or Odd Skillset*:          | *Screenshot of Largest of Two Integers Skillset*:      | *Screenshot of Array and Loops Skillset*:   |
|--------------|-----------|-----------|
| ![Even or Odd Skillset Screenshot](img/even_or_odd.png "My Even or Odd Skillset Screenshot") | ![Largest of Two Integers Skillset Screenshot](img/largest_of_two_integers.png "My Largest of Two Integers Skillset Screenshot")      | ![Array and Loops Skillset Screenshot](img/array_and_loops.png "My Array and Loops Skillset Screenshot")   |

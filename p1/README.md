

# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 1 Requirements:

*Three Parts:*

1. Create Launcher Icon for Both Interfaces
2. Text Shadowing and Bordering
3. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

* Screenshot of first user interface 
* Screenshot of second user interface
* Screenshot of skillset 7 (Random Number Generator with Validation)
* Screenshot of skillset 8 (Largest of Three Integers)
* Screenshot of skillset 9 (Array Runtime Data with Validation)

#### Assignment Screenshots:

| *Screenshot of running application’s first user interface*:          | *Screenshot of running application’s second user interface*:      |
|--------------|-----------|
| ![First Interface Screenshot](img/first_interface.png "My First Interface Screenshot")    | ![Second Interface Screenshot](img/second_interface.png "My Second Interface Screenshot")   |

| *Screenshot of skillset 7 (Random Number Generator with Validation)*:          | *Screenshot of skillset 8 (Largest of Three Integers)*:      | *Screenshot of skillset 9 (Array Runtime Data with Validation)*:   |
|--------------|-----------|-----------|
| ![Skillset 7 Screenshot](img/q7.png "My Random Number Generator w/ Validation")  | ![Skillset 8 Screenshot](img/q8.png "My Largest of Three Integers Screenshot")      | ![Skillset 9 Screenshot](img/q9.png "My Arry Runtime Data w/ Validation Screenshot")   | 

#### Tutorial Links:

*LIS4381 Repo Access Link*
[LIS4381 Repository Link](https://bitbucket.org/dlj19a/lis4381/src/master/ "LIS4381 Repository")

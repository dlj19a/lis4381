import java.util.*;

public class Methods
{   
   public static void getRequirements()
   {
      System.out.println("\nDeveloper: Daniell Jackson\n");
      System.out.println("Program evaluates integers as even or odd.\n");
      System.out.println("Note: Program does *not* check for non-numeric numbers.\n");
   }
   
   public static void evaluateNumber()
   {
      int integer; 
      
      System.out.print("Enter Integer: ");
      Scanner scnr = new Scanner(System.in);
      integer = scnr.nextInt();

      if ((integer % 2) == 0)
      {
         System.out.println(integer + " is an even number.");
      }
      else 
         System.out.println(integer + " is an odd number.");

     
   }
}
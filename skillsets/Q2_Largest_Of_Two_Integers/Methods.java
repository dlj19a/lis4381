import java.util.*;

public class Methods
{   
   public static void getRequirements()
   {
      System.out.println("\nDeveloper: Daniell Jackson\n");
      System.out.println("Program evaluates largest of two numbers.\n");
      System.out.println("Note: Program does *not* check for non-numeric numbers or non-integer values.\n");
   }
   
   public static void compareNumbers()
   {
    int first_num = 0; 
    int second_num = 0; 

    System.out.print("Enter first integer: \n");
    Scanner scnr = new Scanner(System.in);
    first_num = scnr.nextInt();
    System.out.print("Enter second integer: \n");
    second_num = scnr.nextInt();
    

    if (first_num > second_num)
    {
       System.out.println(first_num + " is larger than " + second_num);
    }
    else if (second_num > first_num)
    {
       System.out.println(second_num + " is larger than " + first_num);
    }
    else
    System.out.println("Integers are equal.");
   
 }
}
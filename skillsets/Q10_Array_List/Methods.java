import java.util.*;

public class Methods
{   

   public static void getRequirements()
   {
      System.out.println("\nDeveloper: Daniell Jackson\n");
      System.out.println("Program populates ArrayList of strings with user-entered animal type values.\n");
      System.out.println("Examples: Polar bear, Guinea pig, dog, cat, bird.\n");
      System.out.println("Program continues to collect user-entered values until user types n. \n ");
      System.out.println("Program dislays ArrayList values after each iteration, as well as size. \n");
   }
   
   public static void createArrayList()
   {
      Scanner sc = new Scanner(System.in);
      ArrayList<String> obj = new ArrayList<String>();
      String myStr = "";
      String choice = "y";
      int num = 0;

      while (choice.equals("y"))
      {

         System.out.print("Enter animal type: ");
         myStr = sc.nextLine();
         obj.add(myStr);
         num = obj.size();
         System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
         System.out.print("\nContinue? Enter y or n: ");
         choice = sc.next().toLowerCase();
         sc.nextLine();
      }



   }
    
    
}

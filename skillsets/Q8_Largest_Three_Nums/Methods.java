import java.util.*;

public class Methods
{   
   public static void getRequirements()
   {
      System.out.println("\nDeveloper: Daniell Jackson\n");
      System.out.println("Program evaluates largest of three integers.\n");
      System.out.println("Note: Program checks for integers and non-numeric values.\n");
   }
   
   public static void validateArraySize()
   {
    Scanner sc = new Scanner(System.in);
    int arraySize = 0;
    
    System.out.print("Please enter array size: ");
    while (!sc.hasNextInt())
    {
       System.out.println("Not valid integer!\n");
       sc.next();
       System.out.print("Please try again. Enter array size: ");
    }
    arraySize = sc.nextInt();
    System.out.println();
    
    return arraySize;
   }
   
   public static void calculateNumbers(int arraySize)
   {
      float sum = 0.0f;
      float average = 0.0F;

      System.out.print("Please enter " + arraySize + " numbers: /n");
      
      float numsArray[] = new float[arraySize];

      for(int i =0; i < arraySize; i++)
      while (!sc.hasNextFloat())
      {
         System.out.println("Enter num: " + (i + 1) + " ");
         sc.next();
         System.out.print("Please try again. Enter num " + (i + 1) + ": ");
      }
      numsArray[i] = sc.nextFloat();
      sum = sum +numsArray[i];
   }
   average = sum / arraySize;
}

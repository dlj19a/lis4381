import java.util.*;

public class Methods
{   
   public static void getRequirements()
   {
      System.out.println("\nDeveloper: Daniell Jackson\n");
      System.out.println("Program prompts user for first name and age, then prints results.\n");
      System.out.println("Create four methods from the following requirements:\n");
      System.out.println("1) getRequirements(): Void method displays program requirements:\n");
      System.out.println("2) getUserInput(): Void method prompts for user input,\n");
      System.out.println("\tthen calls two methods: myVoidMethod() and myValueReturningMethod().\n");
      System.out.println("3) myVoidMethod():\n");
      System.out.println("\ta. Accepts two arguments: String and int.\n");
      System.out.println("\tb. Prints user's first name and age.\n");
      System.out.println("4) myValueReturningMethod():\n");
      System.out.println("\ta. Accepts two arguments: String and int.\n");
      System.out.println("\tb. Returns String containing first name and age.\n");
      
   }
   
   public static void getUserInput()
   {
       String firstName="";
       int userAge = 0;
       String myStr="";
       Scanner sc =new Scanner(System.in);

       System.out.print("Enter first name: ");
       firstName=sc.next();

       System.out.print("Enter age: ");
       userAge=sc.nextInt();

       System.out.println();

       System.out.print("void method call: ");
       myVoidMethod(firstName, userAge);

       System.out.print("value returning method call: ");
       myStr = myValueReturningMethod(firstName, userAge);
       System.out.println(myStr);

     
   }

   public static void myVoidMethod(String first, int age)
   {
      System.out.println(first + " is " + age);
   }

   public static String myValueReturningMethod(String first, int age)
   {
      return first + " is " + age;
   }
}
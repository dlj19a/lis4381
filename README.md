# LIS4381 Dr. Mark K. Jowett

## Daniell Jackson

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    -Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Screenshot of both app interfaces
    - Screenshots of skillsets (1-3)

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Screenshots of skillsets (4-6)

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Work within Local Repository to create Mobile Web Application
    - Provide Screenshots of Carousel Slide and Validation Checks (Passed & Failed)
    - Provide Local LIS4381 Web Application Link
    - Screenshots of skillsets (10-12)

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Testing Server Validation
    - Screenshots of A5 Validation
    - Screenshots of skillsets (13-15)

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Personal Business Card App
    - Screenshot of running application’s first user interface 
    - Screenshot of running application’s second user interface 
    - Screenshot of skillsets (7-9)
  
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Testing Editing and Deletion of Data Entries
    - Provide Screenshots of edited entry and deleted entries
    - Creation of an RSS Feed
    - Provide Local LIS4381 Web Application Link
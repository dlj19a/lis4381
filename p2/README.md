> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 1 Requirements:

*Three Parts:*

1. Edit/Delete Addition to Server Side Validation
2. Creation of RSS Feed on Local Host
3. Chapter Questions (Chs 13, 14)

#### README.md file should include the following items:

* Screenshot of Carousel Slide
* Screenshot of Data Table
* Screenshot of Passed and Failed Manipulation 
* Screenshot of RSS Feed
* Link to LIS4381 Local Web Application


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>



#### Assignment Screenshots:

*Screenshot of Carousel Slide*:

![Carousel Slide Screenshot](img/carousel.png)

*Screenshot of Data Table Pre-Manipulation*:

![Data Table Pre-Manipulation](img/index.png)

*Screenshot of Failed Edit Pre-Update*:

![Failed Edit Screenshot](img/edit_failed.png)

*Screenshot of Failed Edit Validation*:

![Failed Edit Validation Screenshot](img/failed_validation.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed_validation.png)

*Screenshot of Delete Entry Prompt*:

![Delete Entry Prompt Screenshot](img/delete_prompt.png)

*Screenshot of Successfully Deleted Record*:

![Successfully Deleted Record Screenshot](img/deletion.png)

*Screenshot of RSS Feed*:

![RSS Feed Screenshot](img/rss.png)


#### Tutorial Links:

*Link to Local LIS4381 A5:*
[Local Host A5 Link](http://localhost:8080/repos/lis4381/p2/index.php "P2 Local Host Link")


# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 1 Requirements:

*Three Parts:*

1. Creation of Web Application through a local repository
2. PHP manipulation with Validation Checks
3. Chapter Questions (Chs 9, 10, 15)

#### README.md file should include the following items:

* Screenshot of Carousel Slide
* Screenshot of Failed Validation Check
* Screenshot of Passing Validation Check
* Screenshot of Skillsets 10 - 12
* Link to Local LIS4381 Web Application

#### Assignment Screenshots:

| *Screenshot of First Carousel Slide*:          | *Screenshot of Failed Validation Check*:      | *Screenshot of Passed Validation Check*:   |
|--------------|-----------|-----------|
| ![First Carousel Slide Screenshot](img/carousel.png "My First Carousel Slide Screenshot") | ![Failed Validation Check Screenshot](img/failed_validation.png "My Failed Validation Check Screenshot")      | ![Passed Validation Check Screenshot](img/passing_validation.png "My Passed Validation Check Screenshot")   |

| *Screenshot of Array List Skillset*:          | *Screenshot of Alpha Numeric Special Skillset*:      | *Screenshot of Temperature Conversion Skillset*:   |
|--------------|-----------|-----------|
| ![Array List Skillset Screenshot](img/Q10.png "My Array List Skillset Screenshot") | ![Alpha Numeric Special Skillset Screenshot](img/Q11.png "My Alpha Numeric Special Skillset Screenshot")      | ![Temperature Conversion Skillset Screenshot](img/Q12.png "My Temperature Conversion Skillset Screenshot")   |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Local LIS4381 Web Application Link](http://localhost:8080/repos/lis4381/index.php "Local LIS4381 Web Application Link")


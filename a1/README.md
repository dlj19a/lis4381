# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation 
* Screenshot of running java Hello.
* Screenshot of running Android Studio
* Git commands with short descriptions
* Bitbucket repo links: a)this assignment and b) the completed tutorials above

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Fetch from and integrate with another repository or a local branch
6. git pull - Update remote refs along with associated objects
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

| *Screenshot of AMPPS running http://localhost*:          | *Screenshot of running java Hello*:      | *Screenshot of Android Studio - My First App*:   |
|--------------|-----------|-----------|
| ![AMPPS Installation Screenshot](img/ampps.png "My AMPPS PHP Screenshot") | ![JDK Installation Screenshot](img/jdk_install.png "My JDK Installation Running")      | ![Android Studio Installation Screenshot](img/android.png "My First App Running")   |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/dlj19a/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
# LIS 4381 - Mobile Web Application Development

## Daniell Jackson

### Assignment 3 Requirements:

*Three Parts:*

1. Work with Database Relations
2. Creating Mobile Ticket App
3. Skillsets 4, 5, 6

#### README.md file should include the following items:

* Database Requirements 
* Mobile App Running
* Screenshot of Skillset 4 (Decision Structures) running
* Screenshot of skillset 5 (Random Number Generator) running
* Screenshot of skillset 6 (Methods) running

#### Assignment Screenshots:

| *Screenshot of ERD Table*:          | *Screenshot of Pet Table*:      |
|--------------|-----------|
| ![ERD Screenshot](img/e.png "My ERD Table Screenshot") | ![Pet Table Screenshot](img/p.png "My Pet Table Screenshot")      


| *Screenshot of Petstore Table*:   | *Screenshot of Customer Table*:   |
|--------------|-----------|
| ![Petstore Table Screenshot](img/pe.png "My Petstore Table Screenshot")   | ![Customer Table Screenshot](img/c.png "My Customer Table Screenshot")   |

| *Screenshot of Mobile App Interface*:          | *Screenshot of Mobile App Processing*:      |
|--------------|-----------|
| ![Mobile App Interface Screenshot](img/us.png "My Mobile App Interface Screenshot")    | ![Mobile App Interface Screenshot](img/pro.png "My Mobile App Processing Screenshot")   |


| *Screenshot of Decision Structures Skillset Running*:          | *Screenshot of Random Number Generator Skillset Running*:      | *Screenshot of Methods Skillset Running*:   |
|--------------|-----------|-----------|
| ![Decision Structures Screenshot](img/de.png "My Decision Structures Skillset Screenshot")  | ![Random Number Generator Screenshot](img/ran.png "My Random Number Generator Skillset Screenshot")      | ![Methods Screenshot](img/me.png "My Methods Skillset Screenshot")   | 

#### Tutorial Links:

*MySQL ERD Table:*
[MYSQL A3 ERD Link](docs/a3.mwb "A3 ERD") 

*Tutorial: Request to update a teammate's repository:*
[MYSQL A3 Script Link](docs/a3.sql "A3 Script ") 